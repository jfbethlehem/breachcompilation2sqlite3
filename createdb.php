<?php
//First create a files.txt using the following command in the BreachCompilation directory: find data -type f > files.txt
//Then run this script: php createdb.php

$database = "BreachCompilation.sqlite3";
$createQuery = "create table passwords (password_id bigint PRIMARY KEY, firstletter text, secondletter text, username text, password text);";

unlink($database);
$db = new PDO('sqlite:'.$database);
$db->exec($createQuery);

function insertFile($db, $file) {
    global $database;
    $j = 0;
    $firstletter = $secondletter = $username = $password = "";
    $f = fopen($file, "r");
    $stmt = $db->prepare('INSERT INTO passwords (username, password, firstletter, secondletter) VALUES (:username,:password, :firstletter, :secondletter);');
    $ex = explode("/", $file);
    $firstletter = $ex[1];
    $secondletter = $ex[2];
    if($f) {
        while(($line = fgets($f, 4096)) != NULL) {
            if($j % 10000 == 0) {
                echo " $file  $j                                                                \r";
            }
            $data = explode(":", trim($line));
            $username = $data[0];
            if(count($data) > 2) {
                $data[0] = "";
                $password = implode(":", $data);
            }
            else
                $password = $data[1];
            $stmt->bindValue(":username", $username);
            $stmt->bindValue(":password", $password);
            $stmt->bindValue(":firstletter", $firstletter);
            $stmt->bindValue(":secondletter", $secondletter);
            $stmt->execute();
            $j++;
        }
    }
}
$starttime = microtime(true);
$files = file("files.txt");
foreach($files as $file) {
    $db->beginTransaction();
    $file = trim($file);
    insertFile($db, $file);
    $db->commit();
}
$db->close();
$stoptime = microtime(true);
echo "Time elapsed: ".($stoptime - $starttime).PHP_EOL;
?>
